/*=========================================================================

  Program:   CMake - Cross-Platform Makefile Generator
  Module:    $RCSfile: cmVisualStudio10TargetGenerator.h,v $
  Language:  C++
  Date:      $Date: 2009-09-07 14:12:17 $
  Version:   $Revision: 1.7 $

  Copyright (c) 2002 Kitware, Inc., Insight Consortium.  All rights reserved.
  See Copyright.txt or http://www.cmake.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef cmVisualStudioTargetGenerator_h
#define cmVisualStudioTargetGenerator_h
#include "cmStandardIncludes.h"

class cmTarget;
class cmMakefile;
class cmGeneratedFileStream;
class cmGlobalVisualStudio7Generator;
class cmSourceFile;
class cmCustomCommand;
class cmLocalVisualStudio7Generator;
class cmComputeLinkInformation;
#include "cmSourceGroup.h"

class cmVisualStudio10TargetGenerator
{
public:
  cmVisualStudio10TargetGenerator(cmTarget* target, 
                                  cmGlobalVisualStudio7Generator* gg);
  ~cmVisualStudio10TargetGenerator();
  void Generate();
  // used by cmVisualStudioGeneratorOptions 
  void WritePlatformConfigTag( 
    const char* tag,
    const char* config, 
    int indentLevel,
    const char* attribute = 0,
    const char* end = 0,
    std::ostream* strm = 0
    );
  
private:
  void ConvertToWindowsSlash(std::string& s);
  void WriteString(const char* line, int indentLevel);
  void WriteProjectConfigurations();
  void WriteProjectConfigurationValues();
  void WriteCLSources();
  void WriteObjSources();
  void WritePathAndIncrementalLinkOptions();
  void WriteItemDefinitionGroups();
  void WriteClOptions(std::string const& config,
                      std::vector<std::string> const & includes);
  void WriteRCOptions(std::string const& config,
                      std::vector<std::string> const & includes);
  void WriteLinkOptions(std::string const& config);
  void WriteMidlOptions(std::string const& config,
                        std::vector<std::string> const & includes);
  void OutputIncludes(std::vector<std::string> const & includes);
  void OutputLinkIncremental(std::string const& configName);
  void WriteCustomRule(cmSourceFile* source,
                       cmCustomCommand const & command);
  void WriteCustomCommands();
  void WriteCustomCommand(cmSourceFile* sf);
  void WriteGroups();
  void WriteProjectReferences();
  bool OutputSourceSpecificFlags(cmSourceFile* source);
  void AddLibraries(cmComputeLinkInformation& cli, std::string& libstring);
  void WriteLibOptions(std::string const& config);
  void WriteEvents(std::string const& configName);
  void WriteEvent(const char* name, std::vector<cmCustomCommand> & commands,
                  std::string const& configName);
  void ComputeObjectNames();
  void WriteGroupSources(const char* name,
                         std::vector<cmSourceFile*> const& sources,
                         std::vector<cmSourceGroup>& );
private:
  std::string ModuleDefinitionFile;
  std::string PathToVcxproj;
  cmTarget* Target;
  cmMakefile* Makefile;
  std::string Platform;
  std::string GUID;
  std::string Name;
  cmGlobalVisualStudio7Generator* GlobalGenerator;
  cmGeneratedFileStream* BuildFileStream;
  cmLocalVisualStudio7Generator* LocalGenerator;
  std::set<cmSourceFile*> SourcesVisited;
};

#endif
